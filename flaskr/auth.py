from flask import Blueprint, render_template, request, flash, redirect, url_for, session
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required, current_user
from .models import User, mdb_session
from .forms import SighUpForm, LoginForm, UserEditProfileForm
import logging

auth = Blueprint("auth", __name__)

@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        # get form data
        email = form.email.data
        password = form.password.data
        ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)

        # query db for user account and match first found(by email address)
        user = mdb_session.query(User).filter_by(email=email).first()
        # if user exists then compare password hashes and log the user in if they match
        if user:
            if check_password_hash(user.password, password):
                flash("Login Successfull!", category="success")
                login_user(user)
                logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) logged in")
                return redirect(url_for("views.home"))
            else:
                flash("Invalid login or password.", category="error")
                logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) tried to log in with incorrect password")
        else:
            #flash("Account does not exist!", category="error")
            logging.info(f"User {email} IP_ADDRESS({ip_addr}) tried to log in with non-existing account")

    return render_template("login.html", user=current_user, form=form)


@auth.route("/logout")
@login_required
def logout():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    logging.info(f"User {current_user.email} IP_ADDRESS({ip_addr}) logged out")
    logout_user()
    return redirect(url_for("views.home"))


@auth.route("/sign-up", methods=["GET", "POST"])
def sign_up():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    form = SighUpForm()
    if form.validate_on_submit():
        email = form.email.data
        first_name = form.first_name.data
        last_name = form.last_name.data
        password = form.password.data

        user = mdb_session.query(User).filter_by(email=email).first()
        if user:
            flash("Login already exists", category="error")
            logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) tried to create account with existing email")
        else:
            new_user = User(
                email=email,
                password=generate_password_hash(password, method="scrypt"),
                first_name=first_name,
                last_name=last_name,
            )
            try:
                mdb_session.add(new_user)
                mdb_session.commit()
                logging.info(f"User {new_user.email} IP_ADDRESS({ip_addr}) created")
                login_user(new_user)
                logging.info(f"User {new_user.email} IP_ADDRESS({ip_addr}) logged in")
                flash("Account created", category="success")
            except Exception as e:
                mdb_session.rollback()
                logging.error(f"User {new_user.email} IP_ADDRESS({ip_addr}) tried to create account but failed")
                flash("Account creation failed", category="error")
            return redirect(url_for("views.home"))
    return render_template("sign_up.html", user=current_user, form=form)


@auth.route("/profile", methods=["GET", "POST"])
@login_required
def profile():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    form = UserEditProfileForm(obj=current_user)
    if form.validate_on_submit():
        first_name = form.first_name.data
        last_name = form.last_name.data
        password = form.password.data
        new_password = form.new_password.data
        
        user = mdb_session.query(User).filter_by(email=current_user.email).first()
        if user:
            if check_password_hash(user.password, password):
                try:
                    user.first_name = first_name
                    user.last_name = last_name
                    mdb_session.commit()
                    logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) updated profile")
                except Exception as e:
                    mdb_session.rollback()
                    logging.error(f"User {user.email} IP_ADDRESS({ip_addr}) tried to update profile but failed")
                    flash("Profile update failed!", category="error")
                if new_password:
                    if len(new_password) >= 5:
                        try:
                            user.password = generate_password_hash(new_password, method="scrypt")
                            mdb_session.commit()
                            logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) updated password")
                            flash("Profile updated successfully!", category="success")
                        except Exception as e:
                            mdb_session.rollback()
                            logging.error(f"User {user.email} IP_ADDRESS({ip_addr}) tried to update password but failed")
                            flash("Profile update failed!", category="error")
                    else:
                        flash("Password is too short!", category="error")
                        logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) tried to update password with too short password")
            else:
                flash("Incorrect password!", category="error")
                logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) tried to update profile with incorrect password")
        else:
            flash("Account does not exist!", category="error")
    return render_template("profile.html", user=current_user, form=form)
