import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from flask_login import UserMixin
from ulid import ULID

engine = sqlalchemy.create_engine(
    "mariadb+mariadbconnector://apreoweb:Kalendar12@mariadb:3306/apreo_web"
)

Base = declarative_base()


class User(Base, UserMixin):
    __tablename__ = "user"
    id = sqlalchemy.Column(sqlalchemy.String(26), primary_key=True, default=str(ULID()))
    email = sqlalchemy.Column(sqlalchemy.String(150), unique=True)
    password = sqlalchemy.Column(sqlalchemy.VARCHAR(255))
    first_name = sqlalchemy.Column(sqlalchemy.String(150))
    last_name = sqlalchemy.Column(sqlalchemy.String(150))
    user_messages = sqlalchemy.orm.relationship("User_message")
    
    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.id = str(ULID())


class User_message(Base):
    __tablename__ = "user_message"
    id = sqlalchemy.Column(sqlalchemy.String(26), primary_key=True, default=str(ULID()))
    data = sqlalchemy.Column(sqlalchemy.String(10000))
    date = sqlalchemy.Column(
        sqlalchemy.DateTime(timezone=True), default=sqlalchemy.func.now()
    )
    user_id = sqlalchemy.Column(sqlalchemy.String(26), sqlalchemy.ForeignKey("user.id"))
    
    def __init__(self, **kwargs):
        super(User_message, self).__init__(**kwargs)
        self.id = str(ULID())


class Anon_user_message(Base):
    __tablename__ = "anon_user_message"
    anon_id = sqlalchemy.Column(sqlalchemy.String(26), primary_key=True, default=str(ULID()))
    anon_email = sqlalchemy.Column(sqlalchemy.String(150), unique=True)
    anon_first_name = sqlalchemy.Column(sqlalchemy.String(150))
    anon_last_name = sqlalchemy.Column(sqlalchemy.String(150))
    anon_data = sqlalchemy.Column(sqlalchemy.String(10000))
    anon_date = sqlalchemy.Column(
        sqlalchemy.DateTime(timezone=True), default=sqlalchemy.func.now()
    )


Base.metadata.create_all(engine)

# Create a session
Session = sqlalchemy.orm.sessionmaker()
Session.configure(bind=engine)
mdb_session = Session()


# Create a user
def create_user(email, password, first_name, last_name):
    new_user = User(
        email=email, password=password, first_name=first_name, last_name=last_name
    )
    mdb_session.add(new_user)
    mdb_session.commit()


# Create an anonymous user with message through contact form
def create_anon_user(email, first_name, last_name, data):
    new_anon_user = Anon_user_message(
        anon_email=email,
        anon_first_name=first_name,
        anon_last_name=last_name,
        anon_data=data,
    )
    mdb_session.add(new_anon_user)
    mdb_session.commit()


# Query for user email in User table
def query_user(email):
    user = mdb_session.query(User).filter_by(email=email).first()
    return user


# Query for user password in User table
def query_user_password(email):
    user = mdb_session.query(User).filter_by(email=email).first()
    return user.password


# Query for user id in User table
def query_user_id(id, UserMixin):
    user = mdb_session.query(User).filter_by(id=id).first()
    return user.id


# Add user message to User_message table
def add_user_message(message, user_id):
    new_message = User_message(data=message, user_id=user_id)
    mdb_session.add(new_message)
    mdb_session.commit()
