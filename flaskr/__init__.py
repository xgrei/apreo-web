from flask import Flask
from flask_login import LoginManager
import logging
import os
from os import path
from dotenv import load_dotenv


# from flask_sqlalchemy import SQLAlchemy

load_dotenv()

def create_apreoweb_app():
    apreoweb_app = Flask(__name__)
    apreoweb_app.config["SECRET_KEY"] = os.getenv("APREOWEB_SECRET_KEY")
    
    from .views import views
    from .auth import auth

    apreoweb_app.register_blueprint(views, url_prefix="/")
    apreoweb_app.register_blueprint(auth, url_prefix="/")

    from .models import User, mdb_session
    
    #Setting env variables
    log_file_name = os.getenv("APREOWEB_LOG_FILE_NAME")
    log_file_level = os.getenv("APREOWEB_LOG_FILE_LEVEL")
    # Set logging level based on environment variable
    if log_file_level == "DEBUG":
        logging.basicConfig(filename=log_file_name, level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(message)s")
    elif log_file_level == "INFO":
        logging.basicConfig(filename=log_file_name, level=logging.INFO, format="%(asctime)s:%(levelname)s:%(message)s")
    elif log_file_level == "WARNING":
        logging.basicConfig(filename=log_file_name, level=logging.WARNING, format="%(asctime)s:%(levelname)s:%(message)s")
    elif log_file_level == "ERROR":
        logging.basicConfig(filename=log_file_name, level=logging.ERROR, format="%(asctime)s:%(levelname)s:%(message)s")
    
    

    login_manager = LoginManager()
    login_manager.login_view = "auth.login"
    login_manager.init_app(apreoweb_app)

    @login_manager.user_loader  # tells flask how to load a user
    def load_user(id):
        return mdb_session.query(User).get(str(id))

    return apreoweb_app