from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, TextAreaField, DateTimeField
from wtforms.validators import (
    Optional,
    ReadOnly,
    DataRequired,
    Email,
    Length,
    EqualTo,
    InputRequired,
)


class SighUpForm(FlaskForm):
    email = StringField(
        label=("Email"), validators=[DataRequired(), Email(), Length(max=120)]
    )
    first_name = StringField(
        label=("First Name"), validators=[DataRequired(), Length(max=20)]
    )
    last_name = StringField(
        label=("Last Name"), validators=[DataRequired(), Length(max=30)]
    )
    password = PasswordField(
        label=("Password"),
        validators=[
            DataRequired(),
            Length(
                min=4, message="Password should be at least %(min)d characters long"
            ),
        ],
    )
    confirm_password = PasswordField(
        label=("Confirm Password"),
        validators=[
            DataRequired(message="*Required"),
            EqualTo("password", message="Both password fields must be equal!"),
        ],
    )
    submit = SubmitField(label=("Sign Up"))


class LoginForm(FlaskForm):
    email = StringField(
        label=("Email"), validators=[DataRequired(), Email(), Length(max=120)]
    )
    password = PasswordField(label=("Password"), validators=[DataRequired()])
    submit = SubmitField(label=("Login"))


class ContactForm(FlaskForm):
    email = StringField(
        label=("Email"), validators=[DataRequired(), Email(), Length(max=120)]
    )
    first_name = StringField(
        label=("First Name"), validators=[DataRequired(), Length(max=20)]
    )
    last_name = StringField(
        label=("Last Name"), validators=[DataRequired(), Length(max=30)]
    )
    contact_message = TextAreaField(
        "Message", validators=[DataRequired(), Length(max=10000)]
    )
    submit = SubmitField(label=("Submit"))


class UserMessageForm(FlaskForm):
    message = TextAreaField("Message", validators=[InputRequired(), Length(max=10000)])
    submit = SubmitField(label=("Send"))

class UserEditProfileForm(FlaskForm):
    email = StringField(
        label=("Email"), 
        validators=[ReadOnly()]
    )
    first_name = StringField(
        label=("First Name"), validators=[DataRequired(), Length(max=20)]
    )
    last_name = StringField(
        label=("Last Name"), validators=[DataRequired(), Length(max=30)]
    )
    password = PasswordField(
        label=("Password"),
        validators=[
            DataRequired()
        ]
    )
    new_password = PasswordField(
        label=("New Password"),
        validators=[
            Optional(),
            EqualTo("confirm_new_password", message="Both password fields must be equal!")
        ]
    )
    confirm_new_password = PasswordField(
        label=("Confirm New Password")
    )
    submit = SubmitField(label=("Update"))
    
