from flask import Blueprint, render_template, request, flash, jsonify, redirect, url_for
from flask_login import login_required, current_user
from .models import User_message, Anon_user_message, User, mdb_session
from .forms import ContactForm, UserMessageForm
import logging

views = Blueprint("views", __name__)


@views.route("/")
def home():
    return render_template("home.html", user=current_user)


@views.route("/contact", methods=["GET", "POST"])
def contact():
    contact_form = ContactForm()
    user_message_form = UserMessageForm()
    auth_user = current_user.is_authenticated
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)

    if auth_user:
        if user_message_form.validate_on_submit():
            user_message = user_message_form.message.data

            new_message = User_message(data=user_message, user_id=current_user.id)
            try:
                mdb_session.add(new_message)
                mdb_session.commit()
                flash("Message Sent!", category="success")
                logging.info(f"User {current_user.email} IP_ADDRESS({ip_addr}) sent a message")
                return redirect(url_for("views.contact"))
            except Exception as e:
                mdb_session.rollback()
                flash("Message failed to send", category="error")
                logging.error(f"User {current_user.email} IP_ADDRESS({ip_addr}) tried to send a message but failed")
    else:
        if contact_form.validate_on_submit():
            email = contact_form.email.data
            first_name = contact_form.first_name.data
            last_name = contact_form.last_name.data
            contact_message = contact_form.contact_message.data

            user = mdb_session.query(User).filter_by(email=email).first()
            if user:
                flash("Email already exists", category="error")
                logging.info(f"User {user.email} IP_ADDRESS({ip_addr}) tried to create account with existing email in contact form")
            else:
                anon_user_message = Anon_user_message(
                    anon_email=email,
                    anon_first_name=first_name,
                    anon_last_name=last_name,
                    anon_data=contact_message,
                )
                try:
                    mdb_session.add(anon_user_message)
                    mdb_session.commit()
                    logging.info(f"Anon user {anon_user_message.anon_email} IP_ADDRESS({ip_addr}) sent a message")
                    flash("Request Sent!", category="success")
                    return redirect(url_for("views.contact"))
                except Exception as e:
                    mdb_session.rollback()
                    logging.error(f"Anon user {anon_user_message.anon_email} IP_ADDRESS({ip_addr}) tried to send a message but failed")
                    flash("Request failed to send", category="error")                    
    return render_template(
        "contact.html",
        user=current_user,
        contact_form=contact_form,
        user_message_form=user_message_form,
    )


@views.route("/references")
def references():
    return render_template("references.html", user=current_user)


@views.route("/messages", methods=["GET", "POST"])
@login_required
def messages():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    if current_user.email == "xgrei@tutanota.com":
        logging.info(f"Admin {current_user.email} IP_ADDRESS({ip_addr}) accessed messages")
        # Query for user messages in Anon_user_message table sorted by date
        anon_user_messages = (
            mdb_session.query(Anon_user_message)
            .order_by(Anon_user_message.anon_date.desc())
            .all()
        )
        users = mdb_session.query(User).all()
        return render_template(
            "messages.html", user=current_user, users=users, anon_user_messages=anon_user_messages
        )
    else:
        return redirect(url_for("views.home"))
