# apreo-web

## Name
Contractors website

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Prerequisites

- Docker
- Docker Compose

## Installation

1. Clone the repository

   Open your terminal and run the following command:

   ```bash
   git clone https://gitlab.com/xgrei/apreo-web.git

2. Navigate to the project directory

    cd apreo-web

3. Setup .env file

    1. Rename or copy .env.example to .env
    2. Set the APREOWEB_LOG_FILE_NAME, APREOWEB_LOG_FILE_LEVEL and APREOWEB_SECRET_KEY values

3. Build and run the Docker containers

    Use Docker Compose to build and run the application and its services:

        docker-compose up --build -d

        The --build option tells Docker Compose to build the images before starting the containers. The -d option tells Docker Compose to run the containers in the background.

The application should now be running at http://localhost:5000 (or whatever port you've configured in your docker-compose.yml).

To stop the application, use the following command:

    docker-compose down

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
